-- Return customerName from the Philippines
SELECT customerName FROM customers WHERE country = "Philippines";

-- Return customerName from the US
SELECT customerName FROM customers WHERE country = "USA";

-- Return contactLastName and contactFirstName of La Rochelle Gifts
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- Return Product Name and MSRP of product The Titanic
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- Return the first and last name of employee whose email is jfirrelli@classicmodelcars.com
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- Return names of customers who have no registered state
SELECT customerName FROM customers WHERE state IS NULL;

-- Return the firstName and lastName and email of employee whose lastname is Patterson and firstname is Steve
SELECT firstName, lastName, email FROM employees WHERE firstName = "Steve" AND lastName = "Patterson";

-- Return Customer Name, Country, Credit Limit of customers whose countries is not USA and whose credit limits are greater than 3000
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- Return customer numbers of order whose comments contain the string "DHL"
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- Return the product line whose text descriptions mentions the phrase state of the art
SELECT productLine, textDescription FROM productlines WHERE textDescription LIKE "%state of the art%";

-- Return the countries of customers without duplication
SELECT DISTINCT country FROM customers;

-- Return the statuses of orders without duplication
SELECT DISTINCT status FROM orders;

-- Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT customerName FROM customers WHERE country IN ("USA","France","Canada");

-- Join offices and employees and return firstName, lastName, and office's city of employees where their office is in Tokyo
SELECT firstName, lastName, city FROM offices
JOIN employees ON offices.officeCode = employees.officeCode
WHERE city = "Tokyo";

-- Join customer and employees and return the names of customers where the employee who served them is Leslie Thompson
SELECT customerName FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE salesRepEmployeeNumber = "1166";

-- Return the product name and quantity in stock of products that belong to the product line "planes" with stock qty less than 1000
SELECT productName, quantityInStock FROM products
WHERE productLine = "Planes" AND quantityInStock < 1000;

-- Return the customer's name with phonenumber containing +81
SELECT customerName FROM customers WHERE phone LIKE "+81%";

-- Return the number of customers in the UK
SELECT COUNT(country) FROM customers WHERE country = "UK";


-- Stretch Goals

-- Join tables to return the product names and customer name of products ordered by "Baane Mini Imports"
SELECT customerName, productName FROM customers
JOIN orders ON customers.customerNumber = orders.customerNumber
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
JOIN products ON orderdetails.productCode = products.productCode
WHERE customerName = "Baane Mini Imports";

-- customer > customer number
-- orders > order number
-- order details > product code
-- products > product code

-- Join tables to return the last names and first names of employees being supervised by Anthony Bow
SELECT a.employeeNumber AS "Employee ID",a.firstName AS "Employee First Name", a.lastName AS "Employee Last Name",
b.employeeNumber AS "Supervisor ID",b.firstName AS "Supervisor First Name", b.lastName AS "Supervisor Last Name"
FROM employees a, employees b
WHERE a.reportsTo = b.employeeNumber AND b.employeeNumber = 1143;


-- Return Product Namr & Product with Highest MSRP
SELECT productName, MAX(MSRP) AS highestMSRP
FROM products;

-- Return the number of products per product line
SELECT productLine, COUNT(productName)
FROM products
GROUP BY productLine;

-- Return the number of cancelled orders
SELECT COUNT(status) FROM orders WHERE status = "cancelled";